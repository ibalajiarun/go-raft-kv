package main

import (
	"errors"
	"fmt"
	"github.com/gogo/protobuf/proto"
	"gitlab.com/ibalajiarun/go-raft-kv/transport"
	"gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"go.etcd.io/etcd/raft"
	"go.etcd.io/etcd/raft/raftpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"math"
	"os"
	"sync"
	"time"
)

const (
	// MaxRetryTime is the number of time we try to initiate
	// a grpc connection to a remote raft member
	MaxRetryTime = 3
	// tickInterval is the interval at which the Paxos state machine "ticks".
	tickInterval = 10 * time.Millisecond
)

var (
	defaultLogger = &raft.DefaultLogger{Logger: log.New(os.Stderr, "raft", log.LstdFlags)}

	// ErrConnectionRefused is thrown when a connection is refused to a transport member in the raft
	ErrConnectionRefused = errors.New("connection refused to the transport")
	// ErrConfChangeRefused is thrown when there is an issue with the configuration change
	ErrConfChangeRefused = errors.New("propose configuration change refused")
	// ErrApplyNotSpecified is thrown during the creation of a raft transport when no apply method was provided
	ErrApplyNotSpecified = errors.New("apply method was not specified")
)

// ApplyCommand function can be used and triggered
// every time there is an append entry event
type ApplyCommand func(interface{})

// DefaultNodeConfig returns the default config for a
// raft transport that can be modified and customized
func DefaultNodeConfig() *raft.Config {
	return &raft.Config{
		HeartbeatTick:   100,
		ElectionTick:    300,
		MaxSizePerMsg:   math.MaxUint16,
		MaxInflightMsgs: 256,
		Logger:          defaultLogger,
	}
}

type server struct {
	raft.Node

	id   uint64
	addr string

	cfg *raft.Config

	ctx context.Context

	ticker  *time.Ticker
	cluster *Cluster

	server *transport.Server

	sendC map[uint64]chan raftpb.Message

	storeLock sync.RWMutex
	PStore    map[string]string
	Store     *raft.MemoryStorage

	stopChan  chan struct{}
	pauseChan chan bool
	pauseLock sync.RWMutex
	pause     bool

	apply ApplyCommand

	pendingRequests  map[uint64]chan *transportpb.PutObjectResponse
}

func newServer(host string, id uint64, peerAddrs []string) (*server, error) {
	ps, err := transport.NewServer(host)
	if err != nil {
		return nil, err
	}

	sendC := make(map[uint64]chan raftpb.Message)

	store := raft.NewMemoryStorage()
	cfg := DefaultNodeConfig()

	s := &server{
		id:   id,
		addr: host,

		cfg: &raft.Config{
			ID:              id,
			ElectionTick:    cfg.ElectionTick,
			HeartbeatTick:   cfg.HeartbeatTick,
			Storage:         store,
			MaxSizePerMsg:   cfg.MaxSizePerMsg,
			MaxInflightMsgs: cfg.MaxInflightMsgs,
			Logger:          cfg.Logger,
		},

		ctx: context.Background(),

		ticker:  time.NewTicker(tickInterval),
		cluster: newCluster(),

		server: ps,

		sendC: sendC,

		Store:  store,
		PStore: make(map[string]string),

		stopChan:  make(chan struct{}),
		pauseChan: make(chan bool),

		apply: handler,

		pendingRequests: make(map[uint64]chan *transportpb.PutObjectResponse),
	}

	var peers []raft.Peer
	for i, host := range peerAddrs {
		id := uint64(i + 1)
		peer := &Peer{
			NodeInfo: &transportpb.NodeInfo{
				ID:   id,
				Addr: host,
			},
		}
		s.cluster.AddPeer(peer)

		nInfo, err := proto.Marshal(peer.NodeInfo)
		if err != nil {
			panic(err)
		}
		peers = append(peers, raft.Peer{ID: id, Context: nInfo})
	}

	s.Node = raft.StartNode(s.cfg, peers)

	return s, nil
}

func (s *server) Init() {
	s.Campaign(s.ctx)
}

func (s *server) Join(joinAddr string) {
	ctx, cancel := context.WithTimeout(s.ctx, 3*time.Second)
	defer cancel()

	client, err := transport.NewPeerClient(ctx, joinAddr)
	if err != nil {
		log.Fatal("couldn't initialize client connection")
	}

	info := &transportpb.NodeInfo{
		ID:   s.id,
		Addr: s.addr,
	}

	resp, err := client.JoinRaft(context.Background(), info)
	if err != nil {
		log.Fatalf("could not join: %v", err)
	}

	err = s.RegisterNodes(resp.GetNodes())
	if err != nil {
		log.Fatal(err)
	}

}

func (s *server) Stop() {
	s.ticker.Stop()
	s.server.Stop()
	s.Stop()
}

func (s *server) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		for {
			select {
			case <-s.ticker.C:
				s.Tick()

			case rd := <-s.Ready():
				s.saveToStorage(rd.HardState, rd.Entries, rd.Snapshot)
				//s.send(rd.Messages)
				s.sendAll(ctx, rd.Messages)
				if !raft.IsEmptySnap(rd.Snapshot) {
					s.processSnapshot(rd.Snapshot)
				}
				for _, entry := range rd.CommittedEntries {
					s.process(entry)
					if entry.Type == raftpb.EntryConfChange {
						var cc raftpb.ConfChange
						err := cc.Unmarshal(entry.Data)
						if err != nil {
							log.Fatal("raft: Can't unmarshal configuration change")
						}
						switch cc.Type {
						case raftpb.ConfChangeAddNode:
							s.applyAddNode(cc)
						case raftpb.ConfChangeRemoveNode:
							s.applyRemoveNode(cc)
						}
						s.ApplyConfChange(cc)
					}
				}
				s.Advance()
			case <-s.stopChan:
				s.Stop()
				s.Node = nil
				close(s.stopChan)
				return

			case pause := <-s.pauseChan:
				// FIXME lock hell
				s.SetPaused(pause)
				for s.pause {
					select {
					case pause = <-s.pauseChan:
						s.SetPaused(pause)
					}
				}
				s.pauseLock.Lock()
				// process pending messages
				for m := range s.server.Msgs() {
					var raftMsg raftpb.Message
					err := proto.Unmarshal(m.Content, &raftMsg)
					if err != nil {
						log.Fatal("unable to unmarshall")
					}
					err = s.Step(s.ctx, raftMsg)
					if err != nil {
						log.Fatal("Something went wrong when unpausing the transport")
					}
				}
				//s.rcvmsg = nil
				s.pauseLock.Unlock()
			case <-s.ticker.C:
				s.Tick()
			case m := <-s.server.Msgs():
				var raftMsg raftpb.Message
				err := proto.Unmarshal(m.Content, &raftMsg)
				if err != nil {
					log.Fatal("unable to unmarshall")
				}
				s.Step(ctx, raftMsg)
			case req := <-s.server.JoinRaftRequests():
				res := s.JoinRaft(ctx, req.Info)
				req.ReturnC <- res
			case req := <-s.server.LeaveRaftRequests():
				res := s.LeaveRaft(ctx, req.Info)
				req.ReturnC <- res
			case req := <-s.server.PutObjectRequests():
				s.PutObject(ctx, req.Req, req.ReturnC)
			case req := <-s.server.ListObjectRequests():
				res := s.ListObjects(ctx, req.Req)
				req.ReturnC <- res
			case req := <-s.server.ListMembersRequests():
				res := s.ListMembers(ctx, req.Req)
				req.ReturnC <- res
			case <-ctx.Done():
				return
			}
		}
	}()
	defer s.Stop()
	return s.server.Serve()
}

// Sends a series of messages to members in the raft
func (s *server) send(messages []raftpb.Message) {

}

func (s *server) sendAll(ctx context.Context, msgs []raftpb.Message) error {
	peers := s.cluster.Peers()

	for _, m := range msgs {
		// Process locally
		if m.To == s.id {
			s.Step(s.ctx, m)
			continue
		}

		// If transport is an active raft member send the message
		if _, ok := peers[m.To]; ok {
			s.sendC[m.To] <- m
		}
	}

	return nil
}

func (s *server) stream(ctx context.Context, to uint64, client *transport.PeerClient) (err error) {
	var stream transportpb.PeerTransport_DeliverMessageClient
	defer func() {
		if st, ok := status.FromError(err); ok && st.Code() == codes.Unavailable {
			// If the transport is down, record that it's unavailable so that we
			// dont continue to sent to it.
			log.Printf("detected transport %d unavailable", to)
			s.ReportUnreachable(to)
			//s.unavailClients[to] = struct{}{}
			err = client.Close()
		} else {
			_, err = stream.CloseAndRecv()
		}
	}()
	retries := 0
	for {
		stream, err = client.DeliverMessage(ctx)
		if err != nil {
			if retries > 5 {
				log.Panic(err)
				return
			} else {
				retries++
				time.Sleep(time.Second)
				continue
			}
		}
		for {
			//s.logger.Debugf("waiting for messages to send to %v", to)
			select {
			case m := <-s.sendC[to]:
				//s.logger.Debugf("sending message to %v", to)
				if m.To != to {
					panic("unexpected destination")
				}
				content, err := m.Marshal()
				if err != nil {
					panic(err)
				}
				msg := transportpb.Message{
					Content: content,
				}
				if err := stream.Send(&msg); err != nil {
					log.Panic(err)
				}
			case <-ctx.Done():
				log.Printf("Context finished: %v", ctx.Err())
				return ctx.Err()
			}
		}

	}
}

// StoreLength returns the length of the store
func (s *server) StoreLength() int {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	return len(s.PStore)
}

// applyAddNode is called when we receive a ConfChange
// from a member in the raft cluster, this adds a new
// transport to the existing raft cluster
func (s *server) applyAddNode(conf raftpb.ConfChange) error {
	peer := &transportpb.NodeInfo{}
	err := proto.Unmarshal(conf.Context, peer)
	if err != nil {
		return err
	}
	if s.id != peer.ID {
		s.RegisterNode(peer)
	}
	return nil
}

// applyRemoveNode is called when we receive a ConfChange
// from a member in the raft cluster, this removes a transport
// from the existing raft cluster
func (s *server) applyRemoveNode(conf raftpb.ConfChange) {
	// The leader steps down
	if s.id == s.Leader() && s.id == conf.NodeID {
		s.Stop()
		return
	}
	// If a follower and the leader steps
	// down, Campaign to be the leader
	if conf.NodeID == s.Leader() {
		s.Campaign(s.ctx)
	}
	s.UnregisterNode(conf.NodeID)
}

// Saves a log entry to our Store
func (s *server) saveToStorage(hardState raftpb.HardState, entries []raftpb.Entry, snapshot raftpb.Snapshot) {
	s.Store.Append(entries)

	if !raft.IsEmptyHardState(hardState) {
		s.Store.SetHardState(hardState)
	}

	if !raft.IsEmptySnap(snapshot) {
		s.Store.ApplySnapshot(snapshot)
	}
}

// Process a data entry and optionnally triggers an event
// or a function handler after the entry is processed
func (s *server) process(entry raftpb.Entry) {
	if entry.Type == raftpb.EntryNormal && entry.Data != nil {
		pair := &transportpb.Pair{}
		err := proto.Unmarshal(entry.Data, pair)
		if err != nil {
			log.Fatal("raft: Can't decode key and value sent through raft")
		}

		// Apply the command
		if s.apply != nil {
			s.apply(entry.Data)
		}

		// Put the value into the store
		s.Put(pair.Key, string(pair.Value))

		ret, ok := s.pendingRequests[pair.ID]
		if ok {
			ret <- &transportpb.PutObjectResponse{
				Success: true,
				Error:   "",
			}
		}
	}
}

// Process snapshot is not yet implemented but applies
// a snapshot to handle transport failures and restart
func (s *server) processSnapshot(snapshot raftpb.Snapshot) {
	// TODO
	panic(fmt.Sprintf("Applying snapshot on transport %v is not implemented", s.id))
}

// LeaveRaft sends a configuration change for a transport
// that is willing to abandon its raft cluster membership
func (s *server) LeaveRaft(ctx context.Context, info *transportpb.NodeInfo) *transportpb.LeaveRaftResponse {
	confChange := raftpb.ConfChange{
		ID:      info.ID,
		Type:    raftpb.ConfChangeRemoveNode,
		NodeID:  info.ID,
		Context: []byte(""),
	}

	err := s.ProposeConfChange(s.ctx, confChange)
	if err != nil {
		return &transportpb.LeaveRaftResponse{
			Success: false,
			Error:   ErrConfChangeRefused.Error(),
		}
	}

	return &transportpb.LeaveRaftResponse{
		Success: true,
		Error:   "",
	}
}

//// Send calls 'Step' which advances the raft state
//// machine with the received message
//func (s *server) Send(ctx context.Context, msg *raftpb.Message) (*transportpb.SendResponse, error) {
//	var err error
//
//	if s.IsPaused() {
//		s.pauseLock.Lock()
//		s.rcvmsg = append(s.rcvmsg, *msg)
//		s.pauseLock.Unlock()
//	} else {
//
//		err = s.Step(ctx, *msg)
//		if err != nil {
//			return &transportpb.SendResponse{Error: err.Error()}, nil
//		}
//	}
//
//	return &transportpb.SendResponse{Error: ""}, nil
//}

// ListMembers lists the members in the raft cluster
func (s *server) ListMembers(ctx context.Context, req *transportpb.ListMembersRequest) *transportpb.ListMembersResponse {
	var peers []*transportpb.NodeInfo
	for _, peer := range s.cluster.Peers() {
		peers = append(peers, peer.NodeInfo)
	}

	return &transportpb.ListMembersResponse{Members: peers}
}

func (s *server) registerRequest(id uint64, retC chan transportpb.PutObjectResponse) {

}

// Put proposes and puts a value in the raft cluster
func (s *server) PutObject(ctx context.Context, req *transportpb.PutObjectRequest, resC chan *transportpb.PutObjectResponse) {
	s.pendingRequests[req.Object.ID] = resC

	pair, err := EncodePair(req.Object.ID, req.Object.Key, req.Object.Value)
	if err != nil {
		resC <- &transportpb.PutObjectResponse{
			Success: false,
			Error:   err.Error(),
		}
	}

	// Propose the value to the raft
	err = s.Propose(ctx, pair)
	if err != nil {
		resC <- &transportpb.PutObjectResponse{
			Success: false,
			Error:   err.Error(),
		}
	}
}

// ListObjects list the objects in the raft cluster
func (s *server) ListObjects(ctx context.Context, req *transportpb.ListObjectsRequest) *transportpb.ListObjectsResponse {
	pairs := s.ListPairs()

	return &transportpb.ListObjectsResponse{Objects: pairs}
}

// RemoveNode removes a transport from the raft cluster
func (s *server) RemoveNode(node *Peer) error {
	confChange := raftpb.ConfChange{
		ID:      node.ID,
		Type:    raftpb.ConfChangeRemoveNode,
		NodeID:  node.ID,
		Context: []byte(""),
	}

	err := s.ProposeConfChange(s.ctx, confChange)
	if err != nil {
		return err
	}
	return nil
}

// RegisterNode registers a new transport on the cluster
func (s *server) RegisterNode(node *transportpb.NodeInfo) error {
	var (
		client *transport.PeerClient
		err    error
	)

	//ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	//defer cancel()

	for i := 1; i <= MaxRetryTime; i++ {
		client, err = transport.NewPeerClient(s.ctx, node.Addr)
		if err != nil {
			if i == MaxRetryTime {
				return ErrConnectionRefused
			}
		}
	}

	s.cluster.AddPeer(
		&Peer{
			NodeInfo: node,
			Client:   client,
		},
	)

	s.sendC[node.ID] = make(chan raftpb.Message, 256)
	go s.stream(s.ctx, node.ID, client)

	log.Println("Registered node:", node)

	return nil
}

// RegisterNodes registers a set of nodes in the cluster
func (s *server) RegisterNodes(nodes []*transportpb.NodeInfo) (err error) {
	for _, node := range nodes {
		err = s.RegisterNode(node)
		if err != nil {
			return err
		}
	}

	return nil
}

// UnregisterNode unregisters a transport that has died or
// has gracefully left the raft subsystem
func (s *server) UnregisterNode(id uint64) {
	// Do not unregister yourself
	if s.id == id {
		return
	}

	delete(s.sendC, id)
	s.cluster.Peers()[id].Client.Close()
	s.cluster.RemovePeer(id)
}

// Get returns a value from the PStore
func (s *server) Get(key string) string {
	s.storeLock.RLock()
	defer s.storeLock.RUnlock()
	return s.PStore[key]
}

// Put puts a value in the raft store
func (s *server) Put(key string, value string) {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	s.PStore[key] = value
}

// List lists the pair in the store
func (s *server) ListPairs() []*transportpb.Pair {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	var pairs []*transportpb.Pair
	for k, v := range s.PStore {
		pairs = append(pairs, &transportpb.Pair{Key: k, Value: []byte(v)})
	}
	return pairs
}

// Shutdown stops the raft transport processing loop.
// Calling Shutdown on an already stopped transport
// will result in a deadlock
func (s *server) Shutdown() {
	s.stopChan <- struct{}{}
}

// Pause pauses the raft transport
func (s *server) Pause() {
	s.pauseChan <- true
}

// Resume brings back the raft transport to activity
func (s *server) Resume() {
	s.pauseChan <- false
}

// IsPaused checks if a transport is paused or not
func (s *server) IsPaused() bool {
	s.pauseLock.Lock()
	defer s.pauseLock.Unlock()
	return s.pause
}

// SetPaused sets the switch for the pause mode
func (s *server) SetPaused(pause bool) {
	s.pauseLock.Lock()
	defer s.pauseLock.Unlock()
	s.pause = pause
	//if s.rcvmsg == nil {
	//	s.rcvmsg = make([]raftpb.Message, 0)
	//}
}

// JoinRaft sends a configuration change to nodes to
// add a new member to the raft cluster
func (s *server) JoinRaft(ctx context.Context, info *transportpb.NodeInfo) *transportpb.JoinRaftResponse {
	meta, err := proto.Marshal(info)
	if err != nil {
		log.Fatal("Can't marshal transport: ", info.ID)
	}

	confChange := raftpb.ConfChange{
		ID:      info.ID,
		Type:    raftpb.ConfChangeAddNode,
		NodeID:  info.ID,
		Context: meta,
	}

	err = s.ProposeConfChange(s.ctx, confChange)
	if err != nil {
		return &transportpb.JoinRaftResponse{
			Success: false,
			Error:   ErrConfChangeRefused.Error(),
		}
	}

	var nodes []*transportpb.NodeInfo
	for _, node := range s.cluster.Peers() {
		nodes = append(nodes, &transportpb.NodeInfo{
			ID:   node.ID,
			Addr: node.Addr,
		})
	}

	return &transportpb.JoinRaftResponse{
		Success: true,
		Nodes:   nodes,
		Error:   "",
	}
}

// IsLeader checks if we are the leader or not
func (s *server) IsLeader() bool {
	if s.Status().Lead == s.id {
		return true
	}
	return false
}

// Leader returns the id of the leader
func (s *server) Leader() uint64 {
	return s.Status().Lead
}
