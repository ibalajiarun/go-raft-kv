package main

import (
	"fmt"
	flag "github.com/spf13/pflag"
	"log"
	"os"
)

const (
	hostDesc = "The name of host"
	portDesc = "The port identifies on which port each server " +
		"will be listening on for incoming TCP connections from " +
		"clients. It can take any integer from 1024 to 65535."
	idDesc = "The optional id specifier of this process. Only needed if multiple " +
		"processes in the hostfile are running on the same host, otherwise it can " +
		"be deduced from the hostfile. 0-indexed."
	verboseDesc = "Sets the logging level to verbose."
	joinDesc    = "Join Description"
	peersDesc   = "List of Peers"
)

var (
	help    = flag.Bool("help", false, "")
	verbose = flag.BoolP("verbose", "v", false, verboseDesc)
	host    = flag.StringP("host", "h", "", hostDesc)
	join    = flag.StringP("join", "j", "", joinDesc)
	hostID  = flag.Uint64P("id", "i", 1000, idDesc)
	peers   = flag.StringSliceP("peers", "p", []string{}, peersDesc)
)

func main() {
	flag.CommandLine.MarkHidden("help")
	flag.Parse()
	if *help {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		fmt.Fprint(os.Stderr, flag.CommandLine.FlagUsagesWrapped(120))
		return
	}

	if *host == "" {
		fmt.Println("host flag required")
	}

	var (
		s   *server
		err error
	)

	if len(*peers) < 3 {
		log.Fatal("give at least 3 peer addrs (including self)")
	}

	s, err = newServer(*host, *hostID, *peers)
	if err != nil {
		log.Fatal(err)
	}

	//if len(*join) > 0 {
	//	s.Join(*join)
	//} else {
	//	s.Init()
	//}

	//if *hostID == 1 {
	//	s.Init()
	//}

	s.Run()
}
