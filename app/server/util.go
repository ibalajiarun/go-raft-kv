package main

import (
	"fmt"
	"github.com/gogo/protobuf/proto"
	"github.com/pkg/errors"
	"gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"log"
)

// EncodePair returns a protobuf encoded key/value pair to be sent through raft
func EncodePair(id uint64, key string, value []byte) ([]byte, error) {
	k := proto.String(key)
	pair := &transportpb.Pair{
		ID:    id,
		Key:   *k,
		Value: value,
	}
	data, err := proto.Marshal(pair)
	if err != nil {
		return nil, errors.New("Can't encode key/value using protobuf")
	}
	return data, nil
}

func handler(msg interface{}) {
	// Here: can be a protobuf 'oneof' message
	pair := &transportpb.Pair{}
	err := proto.Unmarshal(msg.([]byte), pair)
	if err != nil {
		log.Fatal("Can't decode key and value sent through raft")
	}
	fmt.Printf("New entry added to logs: [%v = %v]\n", pair.Key, string(pair.Value))
}
