package main

import (
	"go.etcd.io/etcd/raft"
)

type Config struct {
	ID uint64

	RaftCfg *raft.Config

	apply ApplyCommand
}