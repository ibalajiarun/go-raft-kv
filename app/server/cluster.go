package main

import (
	"gitlab.com/ibalajiarun/go-raft-kv/transport"
	"gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"sync"
)

// Cluster represents a set of active
// transport members
type Cluster struct {
	lock sync.RWMutex

	peers map[uint64]*Peer
}

// Peer represents a transport cluster peer
type Peer struct {
	*transportpb.NodeInfo
	Client *transport.PeerClient
}

// newCluster creates a new cluster neighbors
// list for a transport member
func newCluster() *Cluster {
	return &Cluster{
		peers: make(map[uint64]*Peer),
	}
}

// Peers returns the list of peers in the cluster
func (c *Cluster) Peers() map[uint64]*Peer {
	peers := make(map[uint64]*Peer)
	c.lock.RLock()
	for k, v := range c.peers {
		peers[k] = v
	}
	c.lock.RUnlock()
	return peers
}

// AddPeer adds a transport to our neighbors
func (c *Cluster) AddPeer(peer *Peer) {
	c.lock.Lock()
	c.peers[peer.ID] = peer
	c.lock.Unlock()
}

// RemovePeer removes a transport from our neighbors
func (c *Cluster) RemovePeer(id uint64) {
	c.lock.Lock()
	delete(c.peers, id)
	c.lock.Unlock()
}

