package main

import (
	"context"
	"fmt"
	flag "github.com/spf13/pflag"
	"gitlab.com/ibalajiarun/go-raft-kv/transport"
	"gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	hostDesc = "The name of host"
)

var (
	help   = flag.Bool("help", false, "")
	host   = flag.StringP("host", "h", "", hostDesc)
	insert = flag.BoolP("put", "p", false, "put/get")
)

func main() {
	flag.CommandLine.MarkHidden("help")
	flag.Parse()
	if *help {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		fmt.Fprint(os.Stderr, flag.CommandLine.FlagUsagesWrapped(120))
		return
	}

	if *insert {
		put()
	} else {
		list()
	}
}

func list() {
	var (
		err error
	)

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	client, err := transport.NewExternalClient(*host)
	if err != nil {
		log.Fatal("couldn't initialize client connection")
	}

	resp, err := client.ListObjects(ctx, &transportpb.ListObjectsRequest{})
	if err != nil {
		log.Fatalf("Can't list objects in the cluster: %v", err)
	}

	fmt.Println("Keys:")

	for _, obj := range resp.Objects {
		fmt.Println(":", obj.Key, ":", string(obj.Value))
	}
}

func put() {
	var (
		err error
	)

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	kv := flag.Args()[:3]
	fmt.Println(kv)

	client, err := transport.NewExternalClient(*host)
	if err != nil {
		log.Fatal("couldn't initialize client connection")
	}

	id, err := strconv.Atoi(kv[0])
	if err != nil {
		panic(err)
	}
	resp, err := client.PutObject(ctx, &transportpb.PutObjectRequest{Object: &transportpb.Pair{ID: uint64(id), Key: kv[1], Value: []byte(kv[2])}})
	if resp == nil || err != nil {
		log.Fatal("Can't put object in the cluster", err)
	}
}
