package transport

import (
	transpb "gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"google.golang.org/grpc"
)

type ExternalClient struct {
	transpb.KVServiceClient
	*grpc.ClientConn
}

func NewExternalClient(addr string) (*ExternalClient, error) {
	conn, err := grpc.Dial(addr, clientOpts...)
	if err != nil {
		return nil, err
	}
	client := transpb.NewKVServiceClient(conn)
	return &ExternalClient{client, conn}, nil
}
