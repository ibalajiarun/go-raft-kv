package transport

import (
	"gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"io"
	"net"
)

type JoinRaftRequest struct {
	Info    *transportpb.NodeInfo
	ReturnC chan *transportpb.JoinRaftResponse
}

type LeaveRaftRequest struct {
	Info    *transportpb.NodeInfo
	ReturnC chan *transportpb.LeaveRaftResponse
}

type PutObjectRequest struct {
	Req     *transportpb.PutObjectRequest
	ReturnC chan *transportpb.PutObjectResponse
}

type ListObjectRequest struct {
	Req     *transportpb.ListObjectsRequest
	ReturnC chan *transportpb.ListObjectsResponse
}

type ListMembersRequest struct {
	Req     *transportpb.ListMembersRequest
	ReturnC chan *transportpb.ListMembersResponse
}

type Server struct {
	msgC chan *transportpb.Message
	jrC  chan JoinRaftRequest
	lrC  chan LeaveRaftRequest
	poC  chan PutObjectRequest
	loC  chan ListObjectRequest
	lmC  chan ListMembersRequest

	lis        net.Listener
	grpcServer *grpc.Server
}

func NewServer(host string) (*Server, error) {
	lis, err := net.Listen("tcp", host)
	if err != nil {
		return nil, err
	}
	s := &Server{
		msgC: make(chan *transportpb.Message, 16),
		jrC:  make(chan JoinRaftRequest, 100),
		lrC:  make(chan LeaveRaftRequest, 100),
		poC:  make(chan PutObjectRequest, 100),
		loC:  make(chan ListObjectRequest, 100),
		lmC:  make(chan ListMembersRequest, 100),

		lis:        lis,
		grpcServer: grpc.NewServer(),
	}
	transportpb.RegisterPeerTransportServer(s.grpcServer, s)
	transportpb.RegisterKVServiceServer(s.grpcServer, s)
	return s, nil
}

func (s *Server) DeliverMessage(stream transportpb.PeerTransport_DeliverMessageServer, ) error {
	ctx := stream.Context()
	for {
		msg, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				return stream.SendAndClose(&transportpb.Empty{})
			}
			return err
		}
		select {
		case s.msgC <- msg:
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

func (s *Server) JoinRaft(ctx context.Context, info *transportpb.NodeInfo) (*transportpb.JoinRaftResponse, error) {
	ret := make(chan *transportpb.JoinRaftResponse, 1)
	s.jrC <- JoinRaftRequest{
		Info:    info,
		ReturnC: ret,
	}
	select {
	case res := <-ret:
		return res, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *Server) LeaveRaft(ctx context.Context, info *transportpb.NodeInfo) (*transportpb.LeaveRaftResponse, error) {
	ret := make(chan *transportpb.LeaveRaftResponse, 1)
	s.lrC <- LeaveRaftRequest{
		Info:    info,
		ReturnC: ret,
	}
	select {
	case res := <-ret:
		return res, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *Server) PutObject(ctx context.Context, req *transportpb.PutObjectRequest) (*transportpb.PutObjectResponse, error) {
	ret := make(chan *transportpb.PutObjectResponse, 1)
	s.poC <- PutObjectRequest{
		Req:     req,
		ReturnC: ret,
	}
	select {
	case res := <-ret:
		return res, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *Server) ListObjects(ctx context.Context, req *transportpb.ListObjectsRequest) (*transportpb.ListObjectsResponse, error) {
	ret := make(chan *transportpb.ListObjectsResponse, 1)
	s.loC <- ListObjectRequest{
		Req:     req,
		ReturnC: ret,
	}
	select {
	case res := <-ret:
		return res, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *Server) ListMembers(ctx context.Context, req *transportpb.ListMembersRequest) (*transportpb.ListMembersResponse, error) {
	ret := make(chan *transportpb.ListMembersResponse, 1)
	s.lmC <- ListMembersRequest{
		Req:     req,
		ReturnC: ret,
	}
	select {
	case res := <-ret:
		return res, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (s *Server) Msgs() <-chan *transportpb.Message {
	return s.msgC
}

func (s *Server) JoinRaftRequests() <-chan JoinRaftRequest {
	return s.jrC
}

func (s *Server) LeaveRaftRequests() <-chan LeaveRaftRequest {
	return s.lrC
}

func (s *Server) PutObjectRequests() <-chan PutObjectRequest {
	return s.poC
}

func (s *Server) ListObjectRequests() <-chan ListObjectRequest {
	return s.loC
}

func (s *Server) ListMembersRequests() <-chan ListMembersRequest {
	return s.lmC
}

func (s *Server) Serve() error {
	return s.grpcServer.Serve(s.lis)
}

func (s *Server) Stop() {
	close(s.msgC)
	s.grpcServer.Stop()
}
