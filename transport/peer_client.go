package transport

import (
	"context"
	"gitlab.com/ibalajiarun/go-raft-kv/transport/transportpb"
	"google.golang.org/grpc"
)

var clientOpts = []grpc.DialOption{
	grpc.WithInsecure(),
	//grpc.WithBlock(),
}

type PeerClient struct {
	transportpb.PeerTransportClient
	*grpc.ClientConn
}

func NewPeerClient(ctx context.Context, addr string) (*PeerClient, error) {
	conn, err := grpc.DialContext(ctx, addr, clientOpts...)
	if err != nil {
		return nil, err
	}
	client := transportpb.NewPeerTransportClient(conn)
	return &PeerClient{client, conn}, nil
}