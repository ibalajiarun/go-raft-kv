module gitlab.com/ibalajiarun/go-raft-kv

go 1.12

require (
	github.com/coreos/etcd v3.3.12+incompatible // indirect
	github.com/gogo/protobuf v1.2.1
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.3
	go.etcd.io/etcd v3.3.12+incompatible
	golang.org/x/net v0.0.0-20190415214537-1da14a5a36f2
	google.golang.org/grpc v1.20.0
)
